package com.example.first.repositories;

import com.example.first.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends JpaRepository <Book,Long> {
}
