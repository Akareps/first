package com.example.first.services;

import com.example.first.model.Author;
import com.example.first.model.Book;
import com.example.first.repositories.AuthorRepository;
import com.example.first.repositories.BooksRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
@Service
public class BookstoreOperationsService {
    BooksRepository booksRepository;
    BookstoreOperationsService(BooksRepository booksRepository, AuthorRepository authorRepository) {
        this.booksRepository = booksRepository;
        this.authorRepository = authorRepository;
    }
    AuthorRepository authorRepository;
    public void storeBooks(Map<String,Object> payload) {
        Book book = new Book();
        book.setTitle((String) payload.get("book"));
        booksRepository.save(book);
    }
    public void storeAuthors(Map<String,Object> payload) {
        Author author = new Author();
        author.setName((String) payload.get("author"));
        authorRepository.save(author);
    }

}
