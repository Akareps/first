package com.example.first.services;

import org.springframework.stereotype.Service;

@Service
public class Ribbon {
    public int execute(int [] ribbonNums, int n, int k) {

        int numsUpTok[];
        numsUpTok = new int[k+1];
        int pointer1 = 0, numbersFound = 0, minDistance = n+2;

        for(int i=0;i<n;i++) {

            if(numsUpTok[ribbonNums[i]] == 0) {
                numsUpTok[ribbonNums[i]] = 1;
                numbersFound++;
            } else {
                numsUpTok[ribbonNums[i]]++;
            }

            while(numsUpTok[ribbonNums[pointer1]]>1) {
                numsUpTok[ribbonNums[pointer1]]--;
                pointer1++;
            }

            if(numbersFound==k && i-pointer1+1 < minDistance) {
                minDistance = i-pointer1+1;
            }
        }
        return minDistance<n+2 ? minDistance : -1;
    }
}
