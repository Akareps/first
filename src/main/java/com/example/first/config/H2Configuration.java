package com.example.first.config;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Configuration
public class H2Configuration {

    /**
    * Configuration for accessing h2 database through tcp connection
    */
    @Configuration
    public class H2DatabaseConfiguration {
       @Bean
       public Server h2TcpServer() throws SQLException {
           return Server.createTcpServer().start(); // Connection URL: "jdbc:h2:tcp://localhost:9092/mem:`{database-name}`"
       }
    }

}
