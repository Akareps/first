package com.example.first.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @GetMapping(path = "/fetch/{s}/{k}")
    public int getTestData(@PathVariable int s, @PathVariable int k) {
        System.out.println("Hey");
        return s+k;
    }
}