package com.example.first.controller;

import com.example.first.cache.CacheMemory;
import com.example.first.cache.RibbonCacheKey;
import com.example.first.services.Ribbon;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/ribbon/{nums}/{n}/{k}")
public class RibbonController {
    private int value;
    private Ribbon ribbon;
    private CacheMemory cacheMemory;
    private RibbonCacheKey ribbonCacheKey;
    public RibbonController(Ribbon ribbon, CacheMemory cacheMemory) {
        this.ribbon = ribbon;
        this.cacheMemory = cacheMemory;
    }
    @GetMapping
    public int getTestData(@PathVariable int [] nums,@PathVariable int n, @PathVariable int k) {
        ribbonCacheKey = new RibbonCacheKey(n,k,nums);
        System.out.println("Heyyy");
        if(cacheMemory.check(ribbonCacheKey)) {
            System.out.println("yesss");
            return cacheMemory.fetch(ribbonCacheKey);
        }
        else {
            value = ribbon.execute(nums,n,k);
            cacheMemory.store(ribbonCacheKey, value);
            return value;
        }
    }

}
