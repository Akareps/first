package com.example.first.controller;

import com.example.first.model.Author;
import com.example.first.model.Book;
import com.example.first.repositories.AuthorRepository;
import com.example.first.repositories.BooksRepository;
import com.example.first.services.BookstoreOperationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/bookStore")
public class BookStoreController {
    @Autowired
    BooksRepository booksRepository;
    @Autowired
    AuthorRepository authorRepository;
    private  BookstoreOperationsService bookstoreOperationsService;
    public BookStoreController(BookstoreOperationsService bookstoreOperationsService) {
        this.bookstoreOperationsService = bookstoreOperationsService;
    }
    @PostMapping("/storeBook")
    public void storeBook(@RequestBody Map<String,Object> payload) {
        bookstoreOperationsService.storeBooks(payload);
    }
    @GetMapping("/getBooks")
    public List<Book> fetchBooks(){
        return booksRepository.findAll();
    }

    @PostMapping("/storeAuthor")
    public void storeAuthor(@RequestBody Map<String,Object> payload) {
        bookstoreOperationsService.storeAuthors(payload);
    }
    @GetMapping("/getAuthor")
    public List<Author> fetchAuthors(){
        return authorRepository.findAll();
    }
}
