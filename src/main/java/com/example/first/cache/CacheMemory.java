package com.example.first.cache;

import com.example.first.services.Ribbon;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CacheMemory {
    private ConcurrentHashMap<Object, Integer> cacheMap = new ConcurrentHashMap<>();

    public void store(Object object, int value) {
        cacheMap.put(object,value);
    }

    public int fetch(Object object) {
        return cacheMap.get(object);
    }

    public boolean check(Object object) {
        return (cacheMap.containsKey(object));
    }
}
