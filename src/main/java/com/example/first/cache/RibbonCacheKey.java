package com.example.first.cache;

import java.util.Arrays;
import java.util.Objects;

public class RibbonCacheKey {
    private int n,k,Nums[];
       public RibbonCacheKey(int n, int k, int [] Nums) {
           this.n = n;
           this.k = k;
           this.Nums = Nums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RibbonCacheKey that = (RibbonCacheKey) o;
        return n == that.n &&
                k == that.k &&
                Arrays.equals(Nums, that.Nums);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(n, k);
        result = 31 * result + Arrays.hashCode(Nums);
        return result;
    }
}
