package com.example.first.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ribbon.class)
public class RibbonTest {

    @Autowired
    private Ribbon ribbon;


//    @Before
//    public void beforeEachTestMethod() {
//        ribbon = new Ribbon1();
//    }

    @Test
    public void testExecuteShouldBeSuccessfulWhenComputesProperly() {

        int n=10, k=4;
        int ribbonNums[] = {1,2,2,2,2,3,1,3,4,4};

        int result = ribbon.execute(ribbonNums,n,k);

        assertEquals(5,result);
    }

    @Test
    public void testExecuteShouldBeFailureWhenComputesImproperly() {

        int n=10, k=4;
        int ribbonNums[] = {3,2,2,2,2,3,2,3,4,4};

        int result = ribbon.execute(ribbonNums,n,k);

        assertEquals(-1,result);

    }
}